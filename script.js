
	let trainer = {
		name: `Dave Supan`,
		age: 25,
		pokemon: [`pikachu`, `charizard`, `squirtle`, `bulbasaur`],
		friends: {
				kanto: [`Misty`, `Brock`],
				hoenn: [`May`, `Max`]
		},
	}
	console.log(trainer);

	function talk(){
		console.log(`${trainer.pokemon[0]} I choose you.`)
}
	talk()




	function Pokemon(name, lvl, health, attack) {
		this.name = name;
		this.level = lvl;
		this.health = lvl * 2;
		this.attack = lvl * 3;
		this.intro = function(opponent){
			console.log(`Hi I'm ${this.name} and this is ${opponent.name}`)
		}
		this.tackle = function(opponent) {
		console.log(`${this.name} tackled ${opponent.name}`);
		console.log(`${opponent.name} health is now reduced to ${this.health - this.attack}`);
		}
		let newHealth = this.health - this.attack
		this.awake = function(fight) {
			if (newHealth <= 0) {
			console.log(`${fight.name} fainted.`)
		} else {
			console.log(``)
		}
		}
	}
	
	let pikachu = new Pokemon(`pikachu`, 2, 50, 80);
	let geodude = new Pokemon(`geodude`, 5, 100, 60);
	let mewtwo = new Pokemon(`mewtwo`, 10, 500, 100)

	mewtwo.tackle(geodude);
	geodude.awake();
